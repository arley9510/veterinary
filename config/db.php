<?php

$dbHost = YII_ENV_DEV ? getenv('POSTGRES_HOST') : getenv('RDS_HOSTNAME');
$dbName = YII_ENV_DEV ? getenv('POSTGRES_DB') : getenv('RDS_DB_NAME');
$dbUsername = YII_ENV_DEV ? getenv('POSTGRES_USER') : getenv('RDS_USERNAME');
$dbPassword = YII_ENV_DEV ? getenv('POSTGRES_PASSWORD') : getenv('RDS_PASSWORD');

return [
    'class' => yii\db\Connection::class,
    'dsn' => "pgsql:host=$dbHost;dbname=$dbName",
    'username' => $dbUsername,
    'password' => $dbPassword,
    'charset' => 'utf8',

    'enableSchemaCache' => !YII_DEBUG,
];
