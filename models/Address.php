<?php

namespace app\models;

use yii\behaviors\TimestampBehavior;

class Address extends base\Address
{
    public function behaviors()
    {
        return [
            TimestampBehavior::class,
        ];
    }

    public function formName()
    {
        return '';
    }
}
