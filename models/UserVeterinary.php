<?php

namespace app\models;

use yii\behaviors\TimestampBehavior;

class UserVeterinary extends base\UserVeterinary
{
    public function behaviors()
    {
        return [
            TimestampBehavior::class,
        ];
    }

    public function formName()
    {
        return '';
    }
}
