<?php

namespace app\models;

use yii\behaviors\TimestampBehavior;

class PersonLocation extends base\PersonLocation
{
    public function behaviors()
    {
        return [
            TimestampBehavior::class,
        ];
    }

    public function formName()
    {
        return '';
    }
}
