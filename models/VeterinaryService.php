<?php

namespace app\models;

use yii\behaviors\TimestampBehavior;

class VeterinaryService extends base\VeterinaryService
{
    public function behaviors()
    {
        return [
            TimestampBehavior::class,
        ];
    }

    public function formName()
    {
        return '';
    }
}
