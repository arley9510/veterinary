<?php

namespace app\models;

use yii\behaviors\TimestampBehavior;

class Status extends base\Status
{
    public function behaviors()
    {
        return [
            TimestampBehavior::class,
        ];
    }

    public function formName()
    {
        return '';
    }
}
