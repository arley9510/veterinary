<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\base\UserVeterinary;

/**
 * SearchUserVeterinary represents the model behind the search form of `app\models\base\UserVeterinary`.
 */
class SearchUserVeterinary extends UserVeterinary
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'date_at', 'time_at', 'person_id', 'pet_id', 'veterinary_location_id', 'pay_method_id', 'status_id'], 'integer'],
            [['detail'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = UserVeterinary::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'date_at' => $this->date_at,
            'time_at' => $this->time_at,
            'person_id' => $this->person_id,
            'pet_id' => $this->pet_id,
            'veterinary_location_id' => $this->veterinary_location_id,
            'pay_method_id' => $this->pay_method_id,
            'status_id' => $this->status_id,
        ]);

        $query->andFilterWhere(['ilike', 'detail', $this->detail]);

        return $dataProvider;
    }
}
