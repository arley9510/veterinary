<?php

namespace app\models;

use yii\behaviors\TimestampBehavior;

class SubscriptionType extends base\SubscriptionType
{
    public function behaviors()
    {
        return [
            TimestampBehavior::class,
        ];
    }

    public function formName()
    {
        return '';
    }
}
