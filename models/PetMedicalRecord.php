<?php

namespace app\models;

use yii\behaviors\TimestampBehavior;

class PetMedicalRecord extends base\PetMedicalRecord
{
    public function behaviors()
    {
        return [
            TimestampBehavior::class,
        ];
    }

    public function formName()
    {
        return '';
    }
}
