<?php

namespace app\models;

use yii\behaviors\TimestampBehavior;

class Pet extends base\Pet
{
    public function behaviors()
    {
        return [
            TimestampBehavior::class,
        ];
    }

    public function formName()
    {
        return '';
    }
}
