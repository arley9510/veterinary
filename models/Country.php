<?php

namespace app\models;

use yii\behaviors\TimestampBehavior;

class Country extends base\Country
{
    public function behaviors()
    {
        return [
            TimestampBehavior::class,
        ];
    }

    public function formName()
    {
        return '';
    }
}
