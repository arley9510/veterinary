<?php

namespace app\models;

use yii\behaviors\TimestampBehavior;

class PaymentType extends base\PaymentType
{
    public function behaviors()
    {
        return [
            TimestampBehavior::class,
        ];
    }

    public function formName()
    {
        return '';
    }
}
