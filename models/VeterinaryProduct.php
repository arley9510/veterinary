<?php

namespace app\models;

use yii\behaviors\TimestampBehavior;

class VeterinaryProduct extends base\VeterinaryProduct
{
    public function behaviors()
    {
        return [
            TimestampBehavior::class,
        ];
    }

    public function formName()
    {
        return '';
    }
}
