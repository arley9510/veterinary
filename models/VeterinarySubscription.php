<?php

namespace app\models;

use yii\behaviors\TimestampBehavior;

class VeterinarySubscription extends base\VeterinarySubscription
{
    public function behaviors()
    {
        return [
            TimestampBehavior::class,
        ];
    }

    public function formName()
    {
        return '';
    }
}
