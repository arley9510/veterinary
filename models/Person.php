<?php

namespace app\models;

use yii\behaviors\TimestampBehavior;

class Person extends Base\Person
{
    public function behaviors()
    {
        return [
            TimestampBehavior::class,
        ];
    }

    public function formName()
    {
        return '';
    }
}
