<?php

namespace app\models;

use yii\behaviors\TimestampBehavior;

class DocType extends base\DocType
{
    public function behaviors()
    {
        return [
            TimestampBehavior::class,
        ];
    }

    public function formName()
    {
        return '';
    }
}
