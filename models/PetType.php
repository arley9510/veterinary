<?php

namespace app\models;

use yii\behaviors\TimestampBehavior;

class PetType extends base\PetType
{
    public function behaviors()
    {
        return [
            TimestampBehavior::class,
        ];
    }

    public function formName()
    {
        return '';
    }
}
