<?php

namespace app\models;

use yii\behaviors\TimestampBehavior;

class RoleHasPermission extends base\RoleHasPermission
{
    public function behaviors()
    {
        return [
            TimestampBehavior::class,
        ];
    }

    public function formName()
    {
        return '';
    }
}
