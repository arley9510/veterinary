<?php

namespace app\models;

use yii\behaviors\TimestampBehavior;

class Product extends base\Product
{
    public function behaviors()
    {
        return [
            TimestampBehavior::class,
        ];
    }

    public function formName()
    {
        return '';
    }
}
