<?php

namespace app\models;

use yii\behaviors\TimestampBehavior;

class Permission extends base\Permission
{
    public function behaviors()
    {
        return [
            TimestampBehavior::class,
        ];
    }

    public function formName()
    {
        return '';
    }
}
