<?php

namespace app\models\base;

use Yii;

/**
 * This is the model class for table "veterinary_location".
 *
 * @property int $id
 * @property int $veterinary_id
 * @property int $address_id
 *
 * @property UserVeterinary[] $userVeterinaries
 * @property Address $address
 * @property Veterinary $veterinary
 */
class VeterinaryLocation extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'veterinary_location';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['veterinary_id', 'address_id'], 'required'],
            [['veterinary_id', 'address_id'], 'default', 'value' => null],
            [['veterinary_id', 'address_id'], 'integer'],
            [['address_id'], 'exist', 'skipOnError' => true, 'targetClass' => Address::className(), 'targetAttribute' => ['address_id' => 'id']],
            [['veterinary_id'], 'exist', 'skipOnError' => true, 'targetClass' => Veterinary::className(), 'targetAttribute' => ['veterinary_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'veterinary_id' => 'Veterinary ID',
            'address_id' => 'Address ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserVeterinaries()
    {
        return $this->hasMany(UserVeterinary::className(), ['veterinary_location_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAddress()
    {
        return $this->hasOne(Address::className(), ['id' => 'address_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVeterinary()
    {
        return $this->hasOne(Veterinary::className(), ['id' => 'veterinary_id']);
    }
}
