<?php

namespace app\models\base;

use Yii;

/**
 * This is the model class for table "payment_type".
 *
 * @property int $id
 * @property string $payment_type
 *
 * @property UserVeterinary[] $userVeterinaries
 * @property VeterinarySubscription[] $veterinarySubscriptions
 */
class PaymentType extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'payment_type';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['payment_type'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'payment_type' => 'Payment Type',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserVeterinaries()
    {
        return $this->hasMany(UserVeterinary::className(), ['pay_method_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVeterinarySubscriptions()
    {
        return $this->hasMany(VeterinarySubscription::className(), ['payment_method_id' => 'id']);
    }
}
