<?php

namespace app\models\base;

use Yii;

/**
 * This is the model class for table "product".
 *
 * @property int $id
 * @property string $product
 *
 * @property VeterinaryProduct[] $veterinaryProducts
 */
class Product extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'product';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['product'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'product' => 'Product',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVeterinaryProducts()
    {
        return $this->hasMany(VeterinaryProduct::className(), ['product_id' => 'id']);
    }
}
