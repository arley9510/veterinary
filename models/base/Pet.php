<?php

namespace app\models\base;

use Yii;

/**
 * This is the model class for table "pet".
 *
 * @property int $id
 * @property string $name
 * @property int $person_id
 * @property int $pet_type
 *
 * @property PetType $petType
 * @property User $person
 * @property UserVeterinary[] $userVeterinaries
 */
class Pet extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pet';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['person_id', 'pet_type'], 'required'],
            [['person_id', 'pet_type'], 'default', 'value' => null],
            [['person_id', 'pet_type'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['pet_type'], 'exist', 'skipOnError' => true, 'targetClass' => PetType::className(), 'targetAttribute' => ['pet_type' => 'id']],
            [['person_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['person_id' => 'person_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'person_id' => 'Person ID',
            'pet_type' => 'Pet Type',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPetType()
    {
        return $this->hasOne(PetType::className(), ['id' => 'pet_type']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPerson()
    {
        return $this->hasOne(User::className(), ['person_id' => 'person_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserVeterinaries()
    {
        return $this->hasMany(UserVeterinary::className(), ['pet_id' => 'id']);
    }
}
