<?php

namespace app\models\base;

use Yii;

/**
 * This is the model class for table "veterinary_subscription".
 *
 * @property int $id
 * @property int $costo
 * @property int $subscription_start
 * @property int $subscription_end
 * @property int $veterinary_id
 * @property int $subscription_type_id
 * @property int $payment_method_id
 * @property int $status_id
 *
 * @property PaymentType $paymentMethod
 * @property Status $status
 * @property SubscriptionType $subscriptionType
 * @property Veterinary $veterinary
 */
class VeterinarySubscription extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'veterinary_subscription';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['costo', 'veterinary_id', 'subscription_type_id', 'payment_method_id', 'status_id'], 'required'],
            [['costo', 'subscription_start', 'subscription_end', 'veterinary_id', 'subscription_type_id', 'payment_method_id', 'status_id'], 'default', 'value' => null],
            [['costo', 'subscription_start', 'subscription_end', 'veterinary_id', 'subscription_type_id', 'payment_method_id', 'status_id'], 'integer'],
            [['payment_method_id'], 'exist', 'skipOnError' => true, 'targetClass' => PaymentType::className(), 'targetAttribute' => ['payment_method_id' => 'id']],
            [['status_id'], 'exist', 'skipOnError' => true, 'targetClass' => Status::className(), 'targetAttribute' => ['status_id' => 'id']],
            [['subscription_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => SubscriptionType::className(), 'targetAttribute' => ['subscription_type_id' => 'id']],
            [['veterinary_id'], 'exist', 'skipOnError' => true, 'targetClass' => Veterinary::className(), 'targetAttribute' => ['veterinary_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'costo' => 'Costo',
            'subscription_start' => 'Subscription Start',
            'subscription_end' => 'Subscription End',
            'veterinary_id' => 'Veterinary ID',
            'subscription_type_id' => 'Subscription Type ID',
            'payment_method_id' => 'Payment Method ID',
            'status_id' => 'Status ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentMethod()
    {
        return $this->hasOne(PaymentType::className(), ['id' => 'payment_method_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(Status::className(), ['id' => 'status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubscriptionType()
    {
        return $this->hasOne(SubscriptionType::className(), ['id' => 'subscription_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVeterinary()
    {
        return $this->hasOne(Veterinary::className(), ['id' => 'veterinary_id']);
    }
}
