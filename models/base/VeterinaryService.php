<?php

namespace app\models\base;

use Yii;

/**
 * This is the model class for table "veterinary_service".
 *
 * @property int $id
 * @property int $price
 * @property int $veterinary_id
 * @property int $service_id
 * @property int $status_id
 *
 * @property Service $service
 * @property Status $status
 * @property Veterinary $veterinary
 */
class VeterinaryService extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'veterinary_service';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['price', 'veterinary_id', 'service_id', 'status_id'], 'required'],
            [['price', 'veterinary_id', 'service_id', 'status_id'], 'default', 'value' => null],
            [['price', 'veterinary_id', 'service_id', 'status_id'], 'integer'],
            [['service_id'], 'exist', 'skipOnError' => true, 'targetClass' => Service::className(), 'targetAttribute' => ['service_id' => 'id']],
            [['status_id'], 'exist', 'skipOnError' => true, 'targetClass' => Status::className(), 'targetAttribute' => ['status_id' => 'id']],
            [['veterinary_id'], 'exist', 'skipOnError' => true, 'targetClass' => Veterinary::className(), 'targetAttribute' => ['veterinary_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'price' => 'Price',
            'veterinary_id' => 'Veterinary ID',
            'service_id' => 'Service ID',
            'status_id' => 'Status ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getService()
    {
        return $this->hasOne(Service::className(), ['id' => 'service_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(Status::className(), ['id' => 'status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVeterinary()
    {
        return $this->hasOne(Veterinary::className(), ['id' => 'veterinary_id']);
    }
}
