<?php

namespace app\models\base;

use Yii;

/**
 * This is the model class for table "service".
 *
 * @property int $id
 * @property string $service
 *
 * @property VeterinaryService[] $veterinaryServices
 */
class Service extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'service';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['service'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'service' => 'Service',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVeterinaryServices()
    {
        return $this->hasMany(VeterinaryService::className(), ['service_id' => 'id']);
    }
}
