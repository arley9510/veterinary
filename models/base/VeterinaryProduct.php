<?php

namespace app\models\base;

use Yii;

/**
 * This is the model class for table "veterinary_product".
 *
 * @property int $id
 * @property int $price
 * @property int $veterinary_id
 * @property int $product_id
 * @property int $status_id
 *
 * @property Product $product
 * @property Status $status
 * @property Veterinary $veterinary
 */
class VeterinaryProduct extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'veterinary_product';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['price', 'veterinary_id', 'product_id', 'status_id'], 'required'],
            [['price', 'veterinary_id', 'product_id', 'status_id'], 'default', 'value' => null],
            [['price', 'veterinary_id', 'product_id', 'status_id'], 'integer'],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['product_id' => 'id']],
            [['status_id'], 'exist', 'skipOnError' => true, 'targetClass' => Status::className(), 'targetAttribute' => ['status_id' => 'id']],
            [['veterinary_id'], 'exist', 'skipOnError' => true, 'targetClass' => Veterinary::className(), 'targetAttribute' => ['veterinary_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'price' => 'Price',
            'veterinary_id' => 'Veterinary ID',
            'product_id' => 'Product ID',
            'status_id' => 'Status ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(Status::className(), ['id' => 'status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVeterinary()
    {
        return $this->hasOne(Veterinary::className(), ['id' => 'veterinary_id']);
    }
}
