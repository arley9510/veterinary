<?php

namespace app\models\base;

use Yii;

/**
 * This is the model class for table "user_veterinary".
 *
 * @property int $id
 * @property int $date_at
 * @property int $time_at
 * @property string $detail
 * @property int $person_id
 * @property int $pet_id
 * @property int $veterinary_location_id
 * @property int $pay_method_id
 * @property int $status_id
 *
 * @property PetMedicalRecord[] $petMedicalRecords
 * @property PaymentType $payMethod
 * @property Pet $pet
 * @property Status $status
 * @property User $person
 * @property VeterinaryLocation $veterinaryLocation
 */
class UserVeterinary extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user_veterinary';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['date_at', 'time_at', 'person_id', 'pay_method_id', 'status_id'], 'required'],
            [['date_at', 'time_at', 'person_id', 'pet_id', 'veterinary_location_id', 'pay_method_id', 'status_id'], 'default', 'value' => null],
            [['date_at', 'time_at', 'person_id', 'pet_id', 'veterinary_location_id', 'pay_method_id', 'status_id'], 'integer'],
            [['detail'], 'string'],
            [['pay_method_id'], 'exist', 'skipOnError' => true, 'targetClass' => PaymentType::className(), 'targetAttribute' => ['pay_method_id' => 'id']],
            [['pet_id'], 'exist', 'skipOnError' => true, 'targetClass' => Pet::className(), 'targetAttribute' => ['pet_id' => 'id']],
            [['status_id'], 'exist', 'skipOnError' => true, 'targetClass' => Status::className(), 'targetAttribute' => ['status_id' => 'id']],
            [['person_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['person_id' => 'person_id']],
            [['veterinary_location_id'], 'exist', 'skipOnError' => true, 'targetClass' => VeterinaryLocation::className(), 'targetAttribute' => ['veterinary_location_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date_at' => 'Date At',
            'time_at' => 'Time At',
            'detail' => 'Detail',
            'person_id' => 'Person ID',
            'pet_id' => 'Pet ID',
            'veterinary_location_id' => 'Veterinary Location ID',
            'pay_method_id' => 'Pay Method ID',
            'status_id' => 'Status ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPetMedicalRecords()
    {
        return $this->hasMany(PetMedicalRecord::className(), ['user_veterinary_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPayMethod()
    {
        return $this->hasOne(PaymentType::className(), ['id' => 'pay_method_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPet()
    {
        return $this->hasOne(Pet::className(), ['id' => 'pet_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(Status::className(), ['id' => 'status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPerson()
    {
        return $this->hasOne(User::className(), ['person_id' => 'person_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVeterinaryLocation()
    {
        return $this->hasOne(VeterinaryLocation::className(), ['id' => 'veterinary_location_id']);
    }
}
