<?php

namespace app\models\base;

use Yii;

/**
 * This is the model class for table "permission".
 *
 * @property int $id
 * @property string $permission
 *
 * @property RoleHasPermission[] $roleHasPermissions
 */
class Permission extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'permission';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['permission'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'permission' => 'Permission',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRoleHasPermissions()
    {
        return $this->hasMany(RoleHasPermission::className(), ['permission_id' => 'id']);
    }
}
