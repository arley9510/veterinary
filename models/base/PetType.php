<?php

namespace app\models\base;

use Yii;

/**
 * This is the model class for table "pet_type".
 *
 * @property int $id
 * @property string $pet_type
 *
 * @property Pet[] $pets
 */
class PetType extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pet_type';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['pet_type'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'pet_type' => 'Pet Type',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPets()
    {
        return $this->hasMany(Pet::className(), ['pet_type' => 'id']);
    }
}
