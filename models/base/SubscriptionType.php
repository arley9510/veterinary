<?php

namespace app\models\base;

use Yii;

/**
 * This is the model class for table "subscription_type".
 *
 * @property int $id
 * @property string $subscription_type
 * @property int $cost
 *
 * @property VeterinarySubscription[] $veterinarySubscriptions
 */
class SubscriptionType extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'subscription_type';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cost'], 'default', 'value' => null],
            [['cost'], 'integer'],
            [['subscription_type'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'subscription_type' => 'Subscription Type',
            'cost' => 'Cost',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVeterinarySubscriptions()
    {
        return $this->hasMany(VeterinarySubscription::className(), ['subscription_type_id' => 'id']);
    }
}
