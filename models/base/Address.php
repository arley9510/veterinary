<?php

namespace app\models\base;

use Yii;

/**
 * This is the model class for table "address".
 *
 * @property int $id
 * @property string $address
 * @property string $latitude
 * @property string $longitude
 * @property string $latitudeDelta
 * @property string $longitudeDelta
 * @property int $city_id
 *
 * @property City $city
 * @property PersonLocation[] $personLocations
 * @property VeterinaryLocation[] $veterinaryLocations
 */
class Address extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'address';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['city_id'], 'required'],
            [['city_id'], 'default', 'value' => null],
            [['city_id'], 'integer'],
            [['address', 'latitude', 'longitude', 'latitudeDelta', 'longitudeDelta'], 'string', 'max' => 255],
            [['city_id'], 'exist', 'skipOnError' => true, 'targetClass' => City::className(), 'targetAttribute' => ['city_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'address' => 'Address',
            'latitude' => 'Latitude',
            'longitude' => 'Longitude',
            'latitudeDelta' => 'Latitude Delta',
            'longitudeDelta' => 'Longitude Delta',
            'city_id' => 'City ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::className(), ['id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPersonLocations()
    {
        return $this->hasMany(PersonLocation::className(), ['address_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVeterinaryLocations()
    {
        return $this->hasMany(VeterinaryLocation::className(), ['address_id' => 'id']);
    }
}
