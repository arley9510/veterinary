<?php

namespace app\models\base;

use Yii;

/**
 * This is the model class for table "veterinary".
 *
 * @property int $id
 * @property string $name
 *
 * @property VeterinaryLocation[] $veterinaryLocations
 * @property VeterinaryProduct[] $veterinaryProducts
 * @property VeterinaryService[] $veterinaryServices
 * @property VeterinarySubscription[] $veterinarySubscriptions
 */
class Veterinary extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'veterinary';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVeterinaryLocations()
    {
        return $this->hasMany(VeterinaryLocation::className(), ['veterinary_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVeterinaryProducts()
    {
        return $this->hasMany(VeterinaryProduct::className(), ['veterinary_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVeterinaryServices()
    {
        return $this->hasMany(VeterinaryService::className(), ['veterinary_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVeterinarySubscriptions()
    {
        return $this->hasMany(VeterinarySubscription::className(), ['veterinary_id' => 'id']);
    }
}
