<?php

namespace app\models\base;

use Yii;

/**
 * This is the model class for table "status".
 *
 * @property int $id
 * @property int $status
 * @property string $category
 *
 * @property User[] $users
 * @property UserVeterinary[] $userVeterinaries
 * @property VeterinaryProduct[] $veterinaryProducts
 * @property VeterinaryService[] $veterinaryServices
 * @property VeterinarySubscription[] $veterinarySubscriptions
 */
class Status extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'status';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['status'], 'default', 'value' => null],
            [['status'], 'integer'],
            [['category'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'status' => 'Status',
            'category' => 'Category',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::className(), ['status_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserVeterinaries()
    {
        return $this->hasMany(UserVeterinary::className(), ['status_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVeterinaryProducts()
    {
        return $this->hasMany(VeterinaryProduct::className(), ['status_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVeterinaryServices()
    {
        return $this->hasMany(VeterinaryService::className(), ['status_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVeterinarySubscriptions()
    {
        return $this->hasMany(VeterinarySubscription::className(), ['status_id' => 'id']);
    }
}
