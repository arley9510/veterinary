<?php

namespace app\models\base;

use Yii;

/**
 * This is the model class for table "pet_medical_record".
 *
 * @property int $id
 * @property string $detail
 * @property int $user_veterinary_id
 *
 * @property UserVeterinary $userVeterinary
 */
class PetMedicalRecord extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pet_medical_record';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['detail'], 'string'],
            [['user_veterinary_id'], 'required'],
            [['user_veterinary_id'], 'default', 'value' => null],
            [['user_veterinary_id'], 'integer'],
            [['user_veterinary_id'], 'exist', 'skipOnError' => true, 'targetClass' => UserVeterinary::className(), 'targetAttribute' => ['user_veterinary_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'detail' => 'Detail',
            'user_veterinary_id' => 'User Veterinary ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserVeterinary()
    {
        return $this->hasOne(UserVeterinary::className(), ['id' => 'user_veterinary_id']);
    }
}
