<?php

namespace app\models\base;

use Yii;

/**
 * This is the model class for table "person".
 *
 * @property int $id
 * @property string $first_name
 * @property string $last_name
 * @property int $doc_number
 * @property int $birthday
 * @property string $phone
 * @property string $cell_phone
 * @property int $doc_type
 *
 * @property DocType $docType
 * @property PersonLocation[] $personLocations
 * @property User $user
 */
class Person extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'person';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['doc_number', 'birthday', 'doc_type'], 'default', 'value' => null],
            [['doc_number', 'birthday', 'doc_type'], 'integer'],
            [['first_name', 'last_name', 'phone', 'cell_phone'], 'string', 'max' => 255],
            [['doc_type'], 'exist', 'skipOnError' => true, 'targetClass' => DocType::className(), 'targetAttribute' => ['doc_type' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'doc_number' => 'Doc Number',
            'birthday' => 'Birthday',
            'phone' => 'Phone',
            'cell_phone' => 'Cell Phone',
            'doc_type' => 'Doc Type',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocType()
    {
        return $this->hasOne(DocType::className(), ['id' => 'doc_type']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPersonLocations()
    {
        return $this->hasMany(PersonLocation::className(), ['person_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['person_id' => 'id']);
    }
}
