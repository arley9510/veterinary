<?php

namespace app\models\base;

use Yii;

/**
 * This is the model class for table "doc_type".
 *
 * @property int $id
 * @property string $doc_type
 *
 * @property Person[] $people
 */
class DocType extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'doc_type';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['doc_type'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'doc_type' => 'Doc Type',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPeople()
    {
        return $this->hasMany(Person::className(), ['doc_type' => 'id']);
    }
}
