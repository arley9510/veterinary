<?php

namespace app\models;

use yii\behaviors\TimestampBehavior;

class Veterinary extends base\Veterinary
{
    public function behaviors()
    {
        return [
            TimestampBehavior::class,
        ];
    }

    public function formName()
    {
        return '';
    }
}
