<?php

namespace app\models;

use yii\behaviors\TimestampBehavior;

class VeterinaryLocation extends base\VeterinaryLocation
{
    public function behaviors()
    {
        return [
            TimestampBehavior::class,
        ];
    }

    public function formName()
    {
        return '';
    }
}
