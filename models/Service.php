<?php

namespace app\models;

use yii\behaviors\TimestampBehavior;

class Service extends base\Service
{
    public function behaviors()
    {
        return [
            TimestampBehavior::class,
        ];
    }

    public function formName()
    {
        return '';
    }
}
