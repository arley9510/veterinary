<?php

namespace app\models;

use yii\behaviors\TimestampBehavior;

class Role extends base\Role
{
    public function behaviors()
    {
        return [
            TimestampBehavior::class,
        ];
    }

    public function formName()
    {
        return '';
    }
}
