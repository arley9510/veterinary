<?php

namespace app\models;

use yii\behaviors\TimestampBehavior;

class City extends base\City
{
    public function behaviors()
    {
        return [
            TimestampBehavior::class,
        ];
    }

    public function formName()
    {
        return '';
    }
}
