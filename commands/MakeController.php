<?php

namespace app\commands;

use Yii;
use yii\console\Controller;

/**
 * Class MakeController
 * @package app\commands
 */
class MakeController extends Controller
{
    /**
     * @throws \yii\base\InvalidRouteException
     * @throws \yii\console\Exception
     */
    public function actionAll()
    {
        Yii::$app->runAction('gii/model', [
            'tableName' => '*',
            'ns' => "app\models\base",
            'interactive' => 0
        ]);

        $modelsDirectory = realpath(__DIR__ . '/../models');
        $files = scandir($modelsDirectory);

        foreach ($files as $file) {
            if (is_file("$modelsDirectory/$file")) {
                $className = str_replace('.php', '', $file);

                Yii::$app->runAction('gii/crud', [
                    'controllerClass' => "app\controllers\\" . $className . "Controller",
                    'modelClass' => "app\models\base\\" . $className,
                    'searchModelClass' => "app\models\search\Search" . $className,
                    'interactive' => 0,
                    'overwrite' => 1,
                    'enablePjax' => 1
                ]);
            }
        }
    }
}
