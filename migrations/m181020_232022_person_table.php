<?php

use yii\db\Migration;

/**
 * Class m181020_232022_person_table
 */
class m181020_232022_person_table extends Migration
{
    public const TABLE_NAME = 'person';

    public function up()
    {
        $this->createTable(self::TABLE_NAME, [
            'id' => $this->primaryKey(),
            'first_name' => $this->string(),
            'last_name' => $this->string(),
            'doc_number' => $this->integer(),
            'birthday' => $this->integer(),
            'phone' => $this->string(),
            'cell_phone' => $this->string(),
            'doc_type' => $this->integer(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer()
        ]);

        $this->addForeignKey(
            'doc_type_fk_person',
            self::TABLE_NAME,
            'doc_type',
            'doc_type',
            'id'
        );
    }

    public function down()
    {
        $this->dropTable(self::TABLE_NAME);
    }
}
