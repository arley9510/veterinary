<?php

use yii\db\Migration;

/**
 * Class m181021_005743_veterinary_table
 */
class m181021_005743_veterinary_table extends Migration
{
    public const TABLE_NAME = 'veterinary';

    public function up()
    {
        $this->createTable(self::TABLE_NAME, [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'detail' => $this->text(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer()
        ]);
    }

    public function down()
    {
        $this->dropTable(self::TABLE_NAME);
    }
}
