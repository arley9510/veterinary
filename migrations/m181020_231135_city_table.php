<?php

use yii\db\Migration;

/**
 * Class m181020_231135_city_table
 */
class m181020_231135_city_table extends Migration
{
    public const TABLE_NAME = 'city';

    public function up()
    {
        $this->createTable(self::TABLE_NAME, [
           'id' => $this->primaryKey(),
            'city' => $this->string(),
            'country_id' => $this->integer()->notNull(),
        ]);

        $this->addForeignKey(
            'country_fk_city',
            self::TABLE_NAME,
            'country_id',
            'country',
            'id'
        );
    }

    public function down()
    {
        $this->dropTable(self::TABLE_NAME);
    }
}
