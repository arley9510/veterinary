<?php

use yii\db\Migration;

/**
 * Class m181021_010246_veterinary_subscription_table
 */
class m181021_010246_veterinary_subscription_table extends Migration
{
    public const TABLE_NAME = 'veterinary_subscription';

    public function up()
    {
        $this->createTable(self::TABLE_NAME, [
            'id' => $this->primaryKey(),
            'costo' => $this->integer()->notNull(),
            'subscription_start' => $this->integer(),
            'subscription_end' => $this->integer(),
            'veterinary_id' => $this->integer()->notNull(),
            'subscription_type_id' => $this->integer()->notNull(),
            'payment_method_id' => $this->integer()->notNull(),
            'status_id' => $this->integer()->notNull(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer()
        ]);

        $this->addForeignKey(
            'veterinary_fk_veterinary_subscription',
            self::TABLE_NAME,
            'veterinary_id',
            'veterinary',
            'id'
        );

        $this->addForeignKey(
            'subscription_type_fk_veterinary_subscription',
            self::TABLE_NAME,
            'subscription_type_id',
            'subscription_type',
            'id'
        );

        $this->addForeignKey(
            'payment_method_fk_veterinary_subscription',
            self::TABLE_NAME,
            'payment_method_id',
            'payment_type',
            'id'
        );

        $this->addForeignKey(
            'status_fk_veterinary_subscription',
            self::TABLE_NAME,
            'status_id',
            'status',
            'id'
        );
    }

    public function down()
    {
        $this->dropTable(self::TABLE_NAME);
    }
}
