<?php

use yii\db\Migration;

/**
 * Class m181021_002559_subscription_type_table
 */
class m181021_002559_subscription_type_table extends Migration
{
    public const TABLE_NAME = 'subscription_type';

    public function up()
    {
        $this->createTable(self::TABLE_NAME, [
            'id' => $this->primaryKey(),
            'subscription_type' => $this->string(),
            'detail' => $this->text(),
            'cost' => $this->integer(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer()
        ]);
    }

    public function down()
    {
        $this->dropTable(self::TABLE_NAME);
    }
}
