<?php

use yii\db\Migration;

/**
 * Class m181021_030759_permission_table
 */
class m181021_030759_permission_table extends Migration
{
    public const TABLE_NAME = 'permission';

    public function up()
    {
        $this->createTable(self::TABLE_NAME, [
            'id' => $this->primaryKey(),
            'permission' => $this->text(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer()
        ]);
    }

    public function down()
    {
        $this->dropTable(self::TABLE_NAME);
    }
}
