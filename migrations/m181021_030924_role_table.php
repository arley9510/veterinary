<?php

use yii\db\Migration;

/**
 * Class m181021_030924_role_table
 */
class m181021_030924_role_table extends Migration
{
    public const TABLE_NAME = 'role';

    public function up()
    {
        $this->createTable(self::TABLE_NAME, [
            'id' => $this->primaryKey(),
            'role' => $this->text(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer()
        ]);
    }

    public function down()
    {
        $this->dropTable(self::TABLE_NAME);
    }
}
