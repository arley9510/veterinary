<?php

use yii\db\Migration;

/**
 * Class m181021_002258_product_table
 */
class m181021_002258_product_table extends Migration
{
    public const TABLE_NAME = 'product';

    public function up()
    {
        $this->createTable(self::TABLE_NAME, [
            'id' => $this->primaryKey(),
            'product' => $this->string(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer()
        ]);
    }

    public function down()
    {
        $this->dropTable(self::TABLE_NAME);
    }
}
