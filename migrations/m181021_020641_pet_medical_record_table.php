<?php

use yii\db\Migration;

/**
 * Class m181021_020641_pet_medical_record_table
 */
class m181021_020641_pet_medical_record_table extends Migration
{
    public const TABLE_NAME = 'pet_medical_record';

    public function up()
    {
        $this->createTable(self::TABLE_NAME, [
            'id' => $this->primaryKey(),
            'detail' => $this->text(),
            'user_veterinary_id' => $this->integer()->notNull(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer()
        ]);

        $this->addForeignKey(
            'user_veterinary_fk_pet_medical_record',
            self::TABLE_NAME,
            'user_veterinary_id',
            'user_veterinary',
            'id'
        );
    }

    public function down()
    {
        $this->dropTable(self::TABLE_NAME);
    }
}
