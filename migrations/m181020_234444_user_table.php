<?php

use yii\db\Migration;

/**
 * Class m181020_234444_user_table
 */
class m181020_234444_user_table extends Migration
{
    public const TABLE_NAME = 'user';

    public function up(){
        $this->createTable(self::TABLE_NAME, [
            'email' => $this->string()->unique(),
            'password_hash' => $this->string(),
            'password_reset_token' =>$this->string(),
            'permissions' => $this->text(),
            'status_id' => $this->integer()->notNull(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer()
        ]);

        $this->addColumn(
            self::TABLE_NAME,
            'person_id',
            $this->primaryKey()->after('password_reset_token')
        );

        $this->addForeignKey(
            'person_fk_user',
            self::TABLE_NAME,
            'person_id',
            'person',
            'id'
        );

        $this->addForeignKey(
            'status_fk_user',
            self::TABLE_NAME,
            'status_id',
            'status',
            'id'
        );
    }

    public function down()
    {
        $this->dropTable(self::TABLE_NAME);
    }
}
