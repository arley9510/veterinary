<?php

use yii\db\Migration;

/**
 * Class m181021_005620_payment_type_table
 */
class m181021_005620_payment_type_table extends Migration
{
    public const TABLE_NAME = 'payment_type';

    public function up()
    {
        $this->createTable(self::TABLE_NAME, [
            'id' => $this->primaryKey(),
            'payment_type' => $this->string(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer()
        ]);
    }

    public function down()
    {
        $this->dropTable(self::TABLE_NAME);
    }
}
