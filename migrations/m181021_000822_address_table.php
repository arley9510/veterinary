<?php

use yii\db\Migration;

/**
 * Class m181021_000822_address_table
 */
class m181021_000822_address_table extends Migration
{
    public const TABLE_NAME = 'address';

    public function up()
    {
        $this->createTable(self::TABLE_NAME, [
            'id' => $this->primaryKey(),
            'address' => $this->string(),
            'latitude' => $this->string(),
            'longitude' => $this->string(),
            'latitudeDelta' => $this->string(),
            'longitudeDelta' => $this->string(),
            'city_id' => $this->integer()->notNull(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer()
        ]);

        $this->addForeignKey(
            'city_fk_address',
            self::TABLE_NAME,
            'city_id',
            'city',
            'id'
        );
    }

    public function down()
    {
        $this->dropTable(self::TABLE_NAME);
    }
}
