<?php

use yii\db\Migration;

/**
 * Class m181021_001727_person_location_table
 */
class m181021_001727_person_location_table extends Migration
{
    public const TABLE_NAME = 'person_location';

    public function up()
    {
        $this->createTable(self::TABLE_NAME, [
            'id' => $this->primaryKey(),
            'detail' => $this->text(),
            'person_id' => $this->integer()->notNull(),
            'address_id' => $this->integer()->notNull(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer()
        ]);

        $this->addForeignKey(
            'person_fk_person_location',
            self::TABLE_NAME,
            'person_id',
            'person',
            'id'
        );

        $this->addForeignKey(
            'address_fk_person_location',
            self::TABLE_NAME,
            'address_id',
            'address',
            'id'
        );
    }

    public function down()
    {
        $this->dropTable(self::TABLE_NAME);
    }
}
