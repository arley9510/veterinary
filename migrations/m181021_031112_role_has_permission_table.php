<?php

use yii\db\Migration;

/**
 * Class m181021_031112_role_has_permission_table
 */
class m181021_031112_role_has_permission_table extends Migration
{
    public const TABLE_NAME = 'role_has_permission';

    public function up()
    {
        $this->createTable(self::TABLE_NAME, [
            'id' => $this->primaryKey(),
            'role_id' => $this->integer()->notNull(),
            'permission_id' => $this->integer()->notNull(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer()
        ]);

        $this->addForeignKey(
            'role_fk_role_has_permission',
            self::TABLE_NAME,
            'role_id',
            'role',
            'id'
        );

        $this->addForeignKey(
            'permission_fk_role_has_permission',
            self::TABLE_NAME,
            'permission_id',
            'permission',
            'id'
        );
    }

    public function down()
    {
        $this->dropTable(self::TABLE_NAME);
    }
}
