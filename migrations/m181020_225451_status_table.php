<?php

use yii\db\Migration;

/**
 * Class m181020_225451_status_table
 */
class m181020_225451_status_table extends Migration
{
    public const TABLE_NAME = 'status';

    public function up()
    {
        $this->createTable(self::TABLE_NAME, [
           'id' => $this->primaryKey(),
            'status' => $this->smallInteger(),
            'category' => $this->string(),
        ]);
    }

    public function down()
    {
        $this->dropTable(self::TABLE_NAME);
    }
}
