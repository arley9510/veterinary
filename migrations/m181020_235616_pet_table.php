<?php

use yii\db\Migration;

/**
 * Class m181020_235616_pet_table
 */
class m181020_235616_pet_table extends Migration
{
    public const TABLE_NAME = 'pet';

    public function up(){
        $this->createTable(self::TABLE_NAME, [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'person_id' => $this->integer()->notNull(),
            'pet_type' => $this->integer()->notNull(),
        ]);

        $this->addForeignKey(
            'user_fk_pet',
            self::TABLE_NAME,
            'person_id',
            'user',
            'person_id'
        );

        $this->addForeignKey(
            'pet_type_fk_pet',
            self::TABLE_NAME,
            'pet_type',
            'pet_type',
            'id'
        );
    }

    public function down()
    {
        $this->dropTable(self::TABLE_NAME);
    }
}
