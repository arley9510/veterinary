<?php

use yii\db\Migration;

/**
 * Class m181020_230517_country_table
 */
class m181020_230517_country_table extends Migration
{
    public const TABLE_NAME = 'country';

    public function up()
    {
        $this->createTable(self::TABLE_NAME, [
           'id' => $this->primaryKey(),
            'country' => $this->string(),
        ]);
    }

    public function down()
    {
        $this->dropTable(self::TABLE_NAME);
    }
}
