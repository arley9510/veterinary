<?php

use yii\db\Migration;

/**
 * Class m181021_002432_service_table
 */
class m181021_002432_service_table extends Migration
{
    public const TABLE_NAME = 'service';

    public function up()
    {
        $this->createTable(self::TABLE_NAME, [
            'id' => $this->primaryKey(),
            'service' => $this->string(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer()
        ]);
    }

    public function down()
    {
        $this->dropTable(self::TABLE_NAME);
    }
}
