<?php

use yii\db\Migration;

/**
 * Class m181020_234136_pet_type_table
 */
class m181020_234136_pet_type_table extends Migration
{
    public const TABLE_NAME = 'pet_type';

    public function up(){
        $this->createTable(self::TABLE_NAME, [
           'id' => $this->primaryKey(),
           'pet_type' => $this->string(),
        ]);
    }

    public function down()
    {
        $this->dropTable(self::TABLE_NAME);
    }
}
