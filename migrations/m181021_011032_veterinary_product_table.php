<?php

use yii\db\Migration;

/**
 * Class m181021_011032_veterinary_product_table
 */
class m181021_011032_veterinary_product_table extends Migration
{
    public const TABLE_NAME = 'veterinary_product';

    public function up()
    {
        $this->createTable(self::TABLE_NAME, [
            'id' => $this->primaryKey(),
            'detail' => $this->text(),
            'price' => $this->integer()->notNull(),
            'veterinary_id' => $this->integer()->notNull(),
            'product_id' => $this->integer()->notNull(),
            'status_id' => $this->integer()->notNull(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer()
        ]);

        $this->addForeignKey(
            'veterinary_fk_veterinary_product',
            self::TABLE_NAME,
            'veterinary_id',
            'veterinary',
            'id'
        );

        $this->addForeignKey(
            'product_fk_veterinary_product',
            self::TABLE_NAME,
            'product_id',
            'product',
            'id'
        );

        $this->addForeignKey(
            'status_fk_veterinary_product',
            self::TABLE_NAME,
            'status_id',
            'status',
            'id'
        );
    }

    public function down()
    {
        $this->dropTable(self::TABLE_NAME);
    }
}
