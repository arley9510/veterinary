<?php

use yii\db\Migration;

/**
 * Class m181021_011640_user_veterinary_table
 */
class m181021_011640_user_veterinary_table extends Migration
{
    public const TABLE_NAME = 'user_veterinary';

    public function up()
    {
        $this->createTable(self::TABLE_NAME, [
            'id' => $this->primaryKey(),
            'date_at' => $this->integer()->notNull(),
            'time_at' => $this->integer()->notNull(),
            'detail' => $this->text(),
            'person_id' => $this->integer()->notNull(),
            'pet_id' => $this->integer(),
            'veterinary_location_id' => $this->integer(),
            'pay_method_id' => $this->integer()->notNull(),
            'status_id' => $this->integer()->notNull()
        ]);

        $this->addForeignKey(
            'user_fk_user_veterinary',
            self::TABLE_NAME,
            'person_id',
            'user',
            'person_id'
        );

        $this->addForeignKey(
            'pet_fk_user_veterinary',
            self::TABLE_NAME,
            'pet_id',
            'pet',
            'id'
        );

        $this->addForeignKey(
            'veterinary_location_id_fk_user_veterinary',
            self::TABLE_NAME,
            'veterinary_location_id',
            'veterinary_location',
            'id'
        );

        $this->addForeignKey(
            'pay_method_id_fk_user_veterinary',
            self::TABLE_NAME,
            'pay_method_id',
            'payment_type',
            'id'
        );

        $this->addForeignKey(
            'status_fk_user_veterinary',
            self::TABLE_NAME,
            'status_id',
            'status',
            'id'
        );
    }

    public function down()
    {
        $this->dropTable(self::TABLE_NAME);
    }
}
