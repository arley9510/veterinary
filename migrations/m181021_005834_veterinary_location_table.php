<?php

use yii\db\Migration;

/**
 * Class m181021_005834_veterinary_location_table
 */
class m181021_005834_veterinary_location_table extends Migration
{
    public const TABLE_NAME = 'veterinary_location';

    public function up()
    {
        $this->createTable(self::TABLE_NAME, [
            'id' => $this->primaryKey(),
            'detail' => $this->text(),
            'veterinary_id' => $this->integer()->notNull(),
            'address_id' => $this->integer()->notNull(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer()
        ]);

        $this->addForeignKey(
            'veterinary_fk_veterinary_location',
            self::TABLE_NAME,
            'veterinary_id',
            'veterinary',
            'id'
        );

        $this->addForeignKey(
            'address_fk_veterinary_location',
            self::TABLE_NAME,
            'address_id',
            'address',
            'id'
        );
    }

    public function down()
    {
        $this->dropTable(self::TABLE_NAME);
    }
}
