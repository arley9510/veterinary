<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\base\DocType */

$this->title = 'Create Doc Type';
$this->params['breadcrumbs'][] = ['label' => 'Doc Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="doc-type-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
