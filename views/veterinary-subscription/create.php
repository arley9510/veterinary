<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\base\VeterinarySubscription */

$this->title = 'Create Veterinary Subscription';
$this->params['breadcrumbs'][] = ['label' => 'Veterinary Subscriptions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="veterinary-subscription-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
