<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\base\VeterinarySubscription */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="veterinary-subscription-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'costo')->textInput() ?>

    <?= $form->field($model, 'subscription_start')->textInput() ?>

    <?= $form->field($model, 'subscription_end')->textInput() ?>

    <?= $form->field($model, 'veterinary_id')->textInput() ?>

    <?= $form->field($model, 'subscription_type_id')->textInput() ?>

    <?= $form->field($model, 'payment_method_id')->textInput() ?>

    <?= $form->field($model, 'status_id')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
