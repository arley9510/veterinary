<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\base\PersonLocation */

$this->title = 'Update Person Location: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Person Locations', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="person-location-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
