<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\base\PersonLocation */

$this->title = 'Create Person Location';
$this->params['breadcrumbs'][] = ['label' => 'Person Locations', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="person-location-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
