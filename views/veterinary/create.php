<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\base\Veterinary */

$this->title = 'Create Veterinary';
$this->params['breadcrumbs'][] = ['label' => 'Veterinaries', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="veterinary-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
