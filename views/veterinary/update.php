<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\base\Veterinary */

$this->title = 'Update Veterinary: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Veterinaries', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="veterinary-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
