<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\base\VeterinaryLocation */

$this->title = 'Create Veterinary Location';
$this->params['breadcrumbs'][] = ['label' => 'Veterinary Locations', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="veterinary-location-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
