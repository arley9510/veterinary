<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\base\VeterinaryLocation */

$this->title = 'Update Veterinary Location: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Veterinary Locations', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="veterinary-location-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
