<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\base\UserVeterinary */

$this->title = 'Create User Veterinary';
$this->params['breadcrumbs'][] = ['label' => 'User Veterinaries', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-veterinary-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
