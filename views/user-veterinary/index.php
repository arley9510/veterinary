<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\search\SearchUserVeterinary */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'User Veterinaries';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-veterinary-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create User Veterinary', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'date_at',
            'time_at:datetime',
            'detail:ntext',
            'person_id',
            //'pet_id',
            //'veterinary_location_id',
            //'pay_method_id',
            //'status_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
