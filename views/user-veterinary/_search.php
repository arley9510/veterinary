<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\search\SearchUserVeterinary */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-veterinary-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'date_at') ?>

    <?= $form->field($model, 'time_at') ?>

    <?= $form->field($model, 'detail') ?>

    <?= $form->field($model, 'person_id') ?>

    <?php // echo $form->field($model, 'pet_id') ?>

    <?php // echo $form->field($model, 'veterinary_location_id') ?>

    <?php // echo $form->field($model, 'pay_method_id') ?>

    <?php // echo $form->field($model, 'status_id') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
