<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\base\UserVeterinary */

$this->title = 'Update User Veterinary: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'User Veterinaries', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="user-veterinary-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
