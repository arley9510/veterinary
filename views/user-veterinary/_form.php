<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\base\UserVeterinary */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-veterinary-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'date_at')->textInput() ?>

    <?= $form->field($model, 'time_at')->textInput() ?>

    <?= $form->field($model, 'detail')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'person_id')->textInput() ?>

    <?= $form->field($model, 'pet_id')->textInput() ?>

    <?= $form->field($model, 'veterinary_location_id')->textInput() ?>

    <?= $form->field($model, 'pay_method_id')->textInput() ?>

    <?= $form->field($model, 'status_id')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
