<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\base\PetMedicalRecord */

$this->title = 'Create Pet Medical Record';
$this->params['breadcrumbs'][] = ['label' => 'Pet Medical Records', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pet-medical-record-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
