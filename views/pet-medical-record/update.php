<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\base\PetMedicalRecord */

$this->title = 'Update Pet Medical Record: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Pet Medical Records', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="pet-medical-record-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
