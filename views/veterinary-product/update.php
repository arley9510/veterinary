<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\base\VeterinaryProduct */

$this->title = 'Update Veterinary Product: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Veterinary Products', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="veterinary-product-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
