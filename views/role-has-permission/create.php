<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\base\RoleHasPermission */

$this->title = 'Create Role Has Permission';
$this->params['breadcrumbs'][] = ['label' => 'Role Has Permissions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="role-has-permission-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
